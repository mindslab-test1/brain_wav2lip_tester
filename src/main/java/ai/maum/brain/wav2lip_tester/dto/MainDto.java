package ai.maum.brain.wav2lip_tester.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Base64;

public class MainDto {

    @Data
    public static class Parameter implements Serializable {
        private String text;
        private int speaker;
        private String background;
        private int width;
        private int height;

        public byte[] getBackgroundBytes() {
            return Base64.getDecoder().decode(background);
        }
    }

    @Data
    public static class Id {
        private final String id;
    }
}
