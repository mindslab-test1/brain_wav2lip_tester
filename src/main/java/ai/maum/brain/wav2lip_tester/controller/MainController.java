package ai.maum.brain.wav2lip_tester.controller;

import ai.maum.brain.wav2lip_tester.dto.MainDto;
import ai.maum.brain.wav2lip_tester.service.MainService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.IOException;

@Controller
@RequestMapping("/")
@AllArgsConstructor
public class MainController {
    private final MainService mainService;

    @RequestMapping
    public String index() {
        return "main";
    }

    @PostMapping("/register")
    public @ResponseBody MainDto.Id register(@RequestBody MainDto.Parameter request) {
        return mainService.register(request);
    }

    @GetMapping("/generate/{uuid}")
    public StreamingResponseBody generate(@PathVariable("uuid") String id) throws IOException {
        return mainService.generate(id);
    }
}
