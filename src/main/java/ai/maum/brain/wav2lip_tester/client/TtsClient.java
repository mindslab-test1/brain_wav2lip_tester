package ai.maum.brain.wav2lip_tester.client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import maum.brain.tts.NgTtsServiceGrpc;
import maum.brain.tts.TtsMediaResponse;
import maum.brain.tts.TtsRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Iterator;

@Service
public class TtsClient {
    @Value("${tts.remote}")
    private String remote;
    private ManagedChannel managedChannel;
    private NgTtsServiceGrpc.NgTtsServiceBlockingStub stub;

    @PostConstruct
    public void init(){
        managedChannel = ManagedChannelBuilder.forTarget(remote).usePlaintext().build();
        stub = NgTtsServiceGrpc.newBlockingStub(managedChannel);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        managedChannel.shutdown();
    }

    public Iterator<TtsMediaResponse> speak(int speaker, String text) {
        TtsRequest ttsRequest = TtsRequest.newBuilder()
                .setSpeaker(speaker)
                .setText(text)
                .build();
        return stub.speakWav(ttsRequest);
    }
}
