package ai.maum.brain.wav2lip_tester.client;

import com.google.protobuf.ByteString;
import io.grpc.Context;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import maum.brain.tts.TtsMediaResponse;
import maum.brain.wav2lip.BrainWav2Lip;
import maum.brain.wav2lip.Wav2LipGenerationGrpc;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Iterator;

@Service
public class Wav2lipClient {
    @Value("${wav2lip.remote}")
    private String remote;
    private ManagedChannel managedChannel;
    private Wav2LipGenerationGrpc.Wav2LipGenerationStub stub;

    @PostConstruct
    public void init(){
        managedChannel = ManagedChannelBuilder.forTarget(remote).usePlaintext().build();
        stub = Wav2LipGenerationGrpc.newStub(managedChannel);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        managedChannel.shutdown();
    }

    public PipedInputStream  generate(
            Iterator<TtsMediaResponse> audioIter, byte[] background, int width, int height, Context.CancellableContext context) throws IOException {
        PipedOutputStream outputStream = new PipedOutputStream();
        PipedInputStream inputStream = new PipedInputStream(outputStream);
        context.run(() -> {
            StreamObserver<BrainWav2Lip.Wav2LipResult> responseObserver = new StreamObserver<BrainWav2Lip.Wav2LipResult>() {
                @Override
                public void onNext(BrainWav2Lip.Wav2LipResult wav2LipResult) {
                    try {
                        outputStream.write(wav2LipResult.getVideo().toByteArray());
                    } catch (IOException ignored) {
                    }
                }

                @Override
                public void onError(Throwable throwable) {
                    throwable.printStackTrace();
                }

                @Override
                public void onCompleted() {
                    try {
                        outputStream.close();
                    } catch (IOException ignored) {
                    }
                }
            };
            StreamObserver<BrainWav2Lip.Wav2LipInput> requestObserver = stub.determinate(responseObserver);
            try {
                while (audioIter.hasNext()) {
                    TtsMediaResponse ttsMediaResponse = audioIter.next();
                    ByteString data = ttsMediaResponse.getMediaData();
                    BrainWav2Lip.Wav2LipInput audioInput = BrainWav2Lip.Wav2LipInput.newBuilder()
                            .setAudio(data)
                            .build();
                    requestObserver.onNext(audioInput);
                }

                int len;
                byte[] buffer = new byte[1024 * 16];
                ByteArrayInputStream bis = new ByteArrayInputStream(background);
                while( (len = bis.read(buffer, 0, buffer.length)) > 0){
                    ByteString backgroundByteString = ByteString.copyFrom(buffer, 0, len);
                    BrainWav2Lip.Wav2LipInput backgroundInput = BrainWav2Lip.Wav2LipInput.newBuilder()
                            .setBackground(backgroundByteString)
                            .build();
                    requestObserver.onNext(backgroundInput);
                }

                BrainWav2Lip.Wav2LipInput resolutionInput = BrainWav2Lip.Wav2LipInput.newBuilder()
                        .setResolution(
                                BrainWav2Lip.VideoResolution.newBuilder()
                                        .setWidth(width)
                                        .setHeight(height).build())
                        .build();
                requestObserver.onNext(resolutionInput);
            } catch (RuntimeException e) {
                requestObserver.onError(e);
                throw e;
            }
            requestObserver.onCompleted();
        });
        return inputStream;
    }
}
