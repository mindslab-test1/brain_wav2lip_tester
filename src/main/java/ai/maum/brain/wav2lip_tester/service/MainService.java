package ai.maum.brain.wav2lip_tester.service;

import ai.maum.brain.wav2lip_tester.client.TtsClient;
import ai.maum.brain.wav2lip_tester.client.Wav2lipClient;
import ai.maum.brain.wav2lip_tester.dto.MainDto;
import io.grpc.Context;
import lombok.RequiredArgsConstructor;
import maum.brain.tts.TtsMediaResponse;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.IOException;
import java.io.PipedInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
public class MainService {
    private final TtsClient ttsClient;
    private final Wav2lipClient wav2lipClient;
    private final RedisTemplate<String, MainDto.Parameter> redisTemplate;

    public MainDto.Id register(MainDto.Parameter parameter) {
        String id;
        do {
            id = UUID.randomUUID().toString();
        } while (redisTemplate.hasKey(id));
        redisTemplate.opsForValue().set(id, parameter, 1, TimeUnit.HOURS);
        return new MainDto.Id(id);
    }

    public StreamingResponseBody generate(String id) throws IOException {
        MainDto.Parameter parameter = redisTemplate.opsForValue().get(id);
        redisTemplate.delete(id);
        Iterator<TtsMediaResponse> audioIter = ttsClient.speak(parameter.getSpeaker(), parameter.getText());
        Context.CancellableContext context = Context.current().withCancellation();
        PipedInputStream inputStream = wav2lipClient.generate(audioIter, parameter.getBackgroundBytes(), parameter.getWidth(), parameter.getHeight(), context);
        return outputStream -> {
            WritableByteChannel och = Channels.newChannel(outputStream);
            try {
                byte[] buff = new byte[1024 * 16];
                int len;
                while( (len = inputStream.read(buff, 0, buff.length) ) > 0) {
                    och.write(ByteBuffer.wrap(buff, 0, len));
                }
            } finally {
                context.cancel(null);
                context.close();
                och.close();
                outputStream.close();
            }
        };
    }
}
