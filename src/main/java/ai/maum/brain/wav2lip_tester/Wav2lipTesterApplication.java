package ai.maum.brain.wav2lip_tester;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Wav2lipTesterApplication {

    public static void main(String[] args) {
        SpringApplication.run(Wav2lipTesterApplication.class, args);
    }

}
